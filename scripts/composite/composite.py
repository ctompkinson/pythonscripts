#!/usr/bin/env python

import sys, os
import Image

#Allows import, not immediately evaluated
if __name__ == "__main__":

	blankImage = Image.new("RGB", (512, 512), "white")
	firstImage = Image.open("firstImage.jpg")
	secondImage = Image.open("secondImage.jpg")
	thirdImage = Image.open("thirdImage.jpg")
	fourthImage = Image.open("fourthImage.jpg")

	#blankImage.paste(firstImage, (0,0));
	blankImage.paste(secondImage, (200,0));
	blankImage.paste(thirdImage, (200,200));
	blankImage.paste(fourthImage, (0,200));

	blankImage.save("compositeImage.jpg")
