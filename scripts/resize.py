#!/usr/bin/env python

import sys, os
import Image

if __name__ == "__main__":

	if sys.argv[1:] is not None:

		try:
			image = Image.open(sys.argv[1])

			width = 400
			height = 300

			resizedImage = image.resize((width,height), Image.NEAREST)

			resizedImage.save("resized" +sys.argv[1])

			sys.stdout.write("resized" +sys.argv[1])

		except Exception:
			print "Failed"