#!/usr/bin/python2.7

import os,sys
import argparse

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--verbose", help="increase verbosity", action="store_true")
	parser.add_argument("-a", "--add", help="number to add", type=int)
	parser.add_argument("-s", "--subtract", help="number to subtract", type=int)
	args = parser.parse_args()

	output = args.add - args.subtract

	if args.verbose:
		print "the output is {0}".format(output)
	else:
		print "{0}".format(output)
	