#!/usr/bin/python2.7
import os,traceback

fname = "./S4Transfer.txt"
srcdir = "/Users/chris/Music"
dstdir = "/Users/chris/musiccopy"
artists = []
content = []
columns = []
uniqueArtists = []

with open(fname) as f:
    content = f.read().splitlines()

for line in content:
	columns = line.split('\t')

	if len(columns) > 1:
		artists.append(columns[1])

	if artists > 0:
		for i in artists:
			if i not in uniqueArtists:
				uniqueArtists.append(i)

for artist in uniqueArtists:

	src = srcdir+"/"+artist
	dst = dstdir+"/"+artist

	try:
		src = src.encode("utf-8")
		src = src.replace("\x00","")

		dst = dst.encode("utf-8")
		dst = dst.replace("\x00","")

		print "Symlinked \"{0}\" to \"{1}\"".format(src,dst)
		os.symlink(src,dst)
		pass
	except Exception, e:
		print "Failed to make link for: {0}".format(artist)
		print "src is: {0}".format(src)
		print "dst is: {0}".format(dst)
		traceback.print_exc()
	